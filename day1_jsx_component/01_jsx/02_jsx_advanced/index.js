const name = "全蛋张"
const age = 25

function showObj(obj) {
    return JSON.stringify(obj)
}

const myJsx = (
    <div>
        {/* 劳资是JSX注释 劳资的本质是JS注释 所以HTML中不可见 */}
        上面那位爷是JSX注释

        <div>插入一个变量{name}</div>
        <div>插入一个变量{age}</div>
        <div>一年后的年龄{age + 1}</div>
        <div>蛋哥成年了吗?{age > 18 ? "YES" : "NO"}</div>
        <div>插入函数调用：{showObj({ name, age })}</div>
    </div>
)
console.log("typeof jsx=",typeof(myJsx));

ReactDOM.render(myJsx, document.querySelector("#root"))