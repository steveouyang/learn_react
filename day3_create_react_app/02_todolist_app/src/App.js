import logo from "./logo.svg";
import "./App.css";
// import Home from "./components/Home";
import TodoList from "./TodoList";

function App() {
    console.log("logo=", logo);
    return (
        <div className="App">
            <TodoList />
        </div>
    );
}

export default App;
