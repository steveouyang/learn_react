import React, { Component } from "react";
import DataPicker from "../components/DataPicker";
import DataLister from "../components/DataLister";
import { Todo } from "../models";
import "./index.css";

export default class TodoList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list: [],
        };
    }
    render() {
        const dataPickerProps = {
            placeholder: "请输入待办事项",
            btnText: "添加",
            onBtnClick: (data) => {
                console.log("data=", data);
                this.state.list.unshift(new Todo(data));
                this.setState({
                    list: [...this.state.list],
                });
            },
        };

        const dataListerProps = {
            list: this.state.list,
            itemActionText: "完成",
            onItemAction: (index) => {
                this.state.list.splice(index, 1);
                this.setState({
                    list: [...this.state.list],
                });
            },
        };

        return (
            <div>
                <DataPicker {...dataPickerProps} />
                <DataLister {...dataListerProps} />
            </div>
        );
    }
}
