import React, { Component } from 'react'

export default class DataLister extends Component {
    render() {
        return (
            <ul>{
                this.props.list.map(
                    (item, index) => {
                        return (
                            <li key={item.id}>
                                {item.name}
                                <button onClick={() => {
                                    this.props.onItemAction(index)
                                }}>{this.props.itemActionText}</button>
                            </li>)
                    }
                )
            }</ul>
        )
    }
}
