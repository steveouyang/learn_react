import React, { Component } from 'react'

export default class DataPicker extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userInput: ""
        }
    }

    render() {
        return (
            <div>
                <input type="text" placeholder={this.props.placeholder}
                    value={this.state.userInput}
                    onChange={(e) => { this.setState({ userInput: e.target.value }) }}
                />
                <button onClick={
                    () => {
                        this.props.onBtnClick(this.state.userInput)
                    }
                }>{this.props.btnText}</button>
            </div>
        )
    }
}
