export class Todo {
    constructor(name) {
        this.name = name;
        this.id = new Date().getTime();
    }
}

export class User {
    constructor(name, password) {
        this.name = name;
        this.password = password;
    }
}
