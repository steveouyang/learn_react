import { arrayOf, object, func } from "prop-types";
import { Component, Fragment } from "react";

export default class DataLister extends Component {
    static propTypes = {
        list: arrayOf(object).isRequired,
        classNameGetter: func,
        onItemClick: func
    }

    render() {
        return (
            <div className="dl-wrapper">
                <ul>{
                    this.props.list.map(
                        (item, index) => (
                            <li
                                key={index + item.name}
                                className={this.props.classNameGetter(index)}
                                onClick={() => this.props.onItemClick(index)}
                            >{item.name}</li>
                        )
                    )
                }</ul>
            </div>
        )
    }
}