import { func } from "prop-types";
import { Component, Fragment } from "react";

export default class DataPicker extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userInput: "please enter..."
        }
    }

    static propTypes = {
        onUserInput: func.isRequired
    }

    onUserInputChange = (e) => {
        this.setState({
            userInput: e.target.value
        })
    }

    handleSubmit = (e) => {
        this.props.onUserInput(this.state.userInput)
        this.setState({
            userInput: ""
        })
    }

    render() {
        return (
            <div className="dp-wrapper">
                <input type="text" value={this.state.userInput} onChange={this.onUserInputChange} />
                <button onClick={this.handleSubmit}>提交</button>
            </div>
        )
    }
}