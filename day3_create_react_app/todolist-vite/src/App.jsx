import './App.css';
import TodoList from './views/TodoList';

function App() {
  return (
    <div className="App">
      <TodoList></TodoList>
    </div>
  );
}

export default App;
