import { Component } from "react";
import DataLister from "../components/DataLister";
import DataPicker from "../components/DataPicker";
import './TodoList.css'

class TodoList extends Component {
    constructor(props) {
        super(props)//this.props = props

        /* 定义响应式数据 */
        this.state = {
            todoList: [
                { name: "抽小熊猫", done: false },
                { name: "喝古井贡酒", done: false },
                { name: "烫泡面头", done: true },
            ]
        }
    }

    getClassName = (index) => this.state.todoList[index].done ? "done" : ""

    addTodo = (name) => {
        this.setState({
            todoList: [
                { name, done: false },
                ...this.state.todoList
            ]
        })
    }

    switchTodoitemState = (index) => {
        this.setState({
            todoList: this.state.todoList.map(
                (item, i) => i !== index ? item : { ...item, done: !item.done }
            )
        })
    }



    render() {
        return (
            <div className="td-wrapper">
                <h3>TodoList</h3>
                <DataPicker onUserInput={this.addTodo}></DataPicker>
                <DataLister
                    list={this.state.todoList}
                    classNameGetter={this.getClassName}
                    onItemClick={this.switchTodoitemState}
                ></DataLister>
            </div>
        )
    }
}


export default TodoList
