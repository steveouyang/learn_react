import Home from "./components/Home"
import School from "./components/School"
// import ContextDemo from "./ContextDemo"

function App() {
    return (
        <div>
            <Home />
            <School />
            {/* <ContextDemo/> */}
        </div>
    );
}

export default App;
