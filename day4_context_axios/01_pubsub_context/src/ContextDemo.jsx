import React from "react"
// import "./ContextDemo.css"

// Context 可以让我们无须明确地传遍每一个组件，就能将值深入传递进组件树。
// 为当前的 theme 创建一个 context（“light”为默认值）。
const ThemeContext = React.createContext(null)

class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            theme: {
                backgroundColor: "black",
                color: "white"
            }
        }
    }

    render() {
        // 使用一个 Provider 来将当前的 theme 传递给以下的组件树。
        // 无论多深，任何组件都能读取这个值。
        // 在这个例子中，我们将 “dark” 作为当前的值传递下去。
        return (
            <ThemeContext.Provider value={this.state.theme}>
                <Toolbar />
                <hr />
                <button onClick={
                    () => {
                        this.setState({
                            theme: this.state.theme["color"] === "black"
                                ? { backgroundColor: "black", color: "white" }
                                : { backgroundColor: "white", color: "black" }
                        })
                    }
                }>中央直属按钮</button>
            </ThemeContext.Provider >
        );
    }
}

export default App

// 中间的组件再也不必指明往下传递 theme 了。
function Toolbar(props) {
    // 跟ThemeContext.Provider 遥相呼应
    // value即Provider全局透传的值
    // 拿到全局透传的theme之后 再渲染JSX
    return (
        <ThemeContext.Consumer>
            {
                value => (
                    <>
                        <h3 style={value}>劳资是Toolbar</h3>
                        <ThemedButton />
                    </>
                )
            }
        </ThemeContext.Consumer>
    )
}

class ThemedButton extends React.Component {
    // 指定 contextType 读取当前的 theme context。
    // React 会往上找到最近的 theme Provider，然后使用它的值。
    // 在这个例子中，当前的 theme 值为 “dark”。

    render() {
        console.log("ThemedButton this.context", this.context);
        return (<button style={this.context}>ThemedButton</button>);
    }
}

ThemedButton.contextType = ThemeContext