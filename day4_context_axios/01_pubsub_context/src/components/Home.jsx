import { Component, Fragment } from 'react'
import PubSub from "pubsub-js"

export default class Home extends Component {
    render() {
        return (
            <Fragment>
                <h3>Home</h3>

                <button onClick={
                    (e) => {
                        PubSub.publish("mymsg", { msg: "我们正在学习事件总线", timestamp: Date.now() })
                    }
                }>发布事件</button>
                
            </Fragment>
        )
    }
}