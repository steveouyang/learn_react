import { Component } from 'react'
import PubSub from 'pubsub-js'

export default class School extends Component {
    /* 组件挂载完毕时开始订阅消息 */
    componentDidMount() {
        PubSub.subscribe(
            "mymsg",
            (msgType, data) => {
                console.log("收到订阅消息", msgType, data);
            }
        )
    }

    /* 组件被卸载时取消订阅 释放资源 */
    componentWillUnmount() {
        PubSub.unsubscribe("mymsg")
    }

    render() {
        return (
            <>
                <h3>School</h3>
                <button onClick={PubSub.unsubscribe.bind(null, "mymsg")}>取消订阅</button>
            </>
        )
    }
}