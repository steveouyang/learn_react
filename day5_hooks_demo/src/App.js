import "./App.css";
import { useState } from "react";

/* hook demos */
import UseStateDemo from "./demos/ClickCounter";
import UseEffectDemo from "./demos/LifeCycles";
import Practice from "./demos/Practice";
import UseRefDemo from "./demos/UseRefDemo";
import UseContextDemo from "./demos/UseContextDemo";
import UseReducerDemo from "./demos/UseReducerDemo";
import UseMemoDemo from "./demos/UseMemoDemo";
import UseCallbackDemo from "./demos/UseCallbackDemo";
import UseFetchDemo from "./demos/UseFetchDemo";
import UseMousePositionDemo from './demos/UseMousePositionDemo'
import UseClock from "./demos/UseClock";
import HookTrap from "./demos/HookTrap"

function App() {
    const [flag, setFlag] = useState(true);

    function switchFlag() {
        setFlag(!flag);
    }

    const options = {
        headers: {
            "X-Host": "mall.film-ticket.city.list",
        },
    };
    const url = "https://m.maizuo.com/gateway?k=5666469";

    return (
        <div className="App">
            React Hooks <button onClick={switchFlag}>切换flag状态</button>
            <hr />

            {/* <UseStateDemo /> */}
            {/* {flag && <UseEffectDemo />} */}
            {/* {flag && <Practice />} */}
            {/* <UseRefDemo /> */}
            {/* <UseContextDemo /> */}
            {/* <UseReducerDemo /> */}
            {/* <UseMemoDemo /> */}
            {/* <UseCallbackDemo /> */}
            {/* 自定义hook */}
            {/* <UseFetchDemo
                url={url}
                options={options}
            /> */}
            {/* {flag && <UseMousePositionDemo />} */}
            {/* {flag && <UseClock />} */}
            <HookTrap couseName={"React从入门到住院"}/>
        </div>
    );
}

export default App;
