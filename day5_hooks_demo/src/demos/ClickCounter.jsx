// rcc rfc
import React, { useState } from 'react'

export default function ClickCounter(props) {

    // 创建一个number类型的state(响应式数据)
    // 同时得到该数据 + 该数据的设置器
    const [count, setCount] = useState(0)

    // 创建一个string类型的state(响应式数据)
    const [name, setName] = useState("steve")

    /* 更新state => ClickCounter重新调用 */
    function onBtnClick() {
        setCount(count+1)
        setName(name + Date.now())
    }

    return (
        <>
            {/* 显示响应式数据 当stage更新时 ClickCounter组件会自动重新调用 */}
            <div>
                <span>{count}</span>
                <br />
                <span>{name}</span>
            </div>

            {/* 点击按钮重新设置state => 视图刷新 */}
            <button onClick={onBtnClick}>+</button>
        </>
    )

}
