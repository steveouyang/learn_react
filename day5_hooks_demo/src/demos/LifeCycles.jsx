import React, { useState, useEffect } from 'react'

export default function LifeCycles(props) {

    const [time, setTime] = useState(0)
    const [count, setCount] = useState(0)

    // 模拟 ComponentDidMount ComponentDidUpdate
    // 什么都不依赖 事实上相当于全依赖
    useEffect(
        () => {
            console.log("全依赖 ComponentDidMount/ComponentDidUpdate");
        }
    )

    // 模拟 ComponentDidMount ComponentWillUnmount
    useEffect(
        () => {
            console.log("空依赖 ComponentDidMount2");

            // 所依赖的数据发生改变时 会先回调上一次副作用的返回值函数
            // 空依赖副作用的返回值函数 会在组件卸载前执行一次
            return () => {
                console.log("空依赖的返回值函数在组件卸载前执行一次 => ComponentWillUnmount");
            }
        },
        []
    )

    // 有具体依赖的副作用
    // count更新时 副作用会被回调
    useEffect(
        ()=>{
            console.log("依赖count,time","ComponentDidUpdate");
            return ()=>{
                console.log("依赖count,time","下一次count更新时回调上一次count更新的返回值函数");
            }
        },
        [count,time]
    )

    return (
        <div>
            模拟组件生命周期
            <br />
            {time} {" "} 
            <button onClick={() => setTime(Date.now())}>更新时间</button>
            <br />
            {count} {" "} 
            <button onClick={() => setCount(count + 1)}>+</button>
            {console.log("re-rendered")}
        </div>
    )
}
