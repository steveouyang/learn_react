import React, { useState, useEffect, useRef } from 'react'

export default function Practice() {
    const [count, setCount] = useState(0)
    const timerRef = useRef(-1)


    /* 组件挂载时创建定时器 */
    useEffect(
        () => {
            console.log("ComponentDidMount");
            timerRef.current = setInterval(() => {
                setCount(value => value + 1)
                setCount(value => value + 1)
                console.log("timer run");
            }, 1000);

            return () => {
                /* 组件卸载时清除定时器 */
                console.log("ComponentWillUnmount");
                clearInterval(timerRef.current)
            }
        },
        []
    )



    return (
        <div>
            {count}
            {console.log("re-rendered")}
        </div>
    )
}
