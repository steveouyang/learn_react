import React, { useContext, useState } from 'react'

function Dog() {
    // 孙子组件获取全局透传的Context数据
    const theme = useContext(ThemeContext)
    console.log("theme",theme);
    return (
        // 根据主题不同样式的渲染JSX
        <div style={theme}>我是一只狗狗</div>
    )
}

function Son() {
    return (
        <div>
            我是儿子
            <Dog />
        </div>
    )
}

const themes = {
    light: {
        backgroundColor: "white",
        color: "black"
    },
    dark: {
        backgroundColor: "black",
        color: "white"
    }
}

// 创建Context对象
const ThemeContext = React.createContext(themes.dark)

function UseContextDemo(props) {
    // 全局主题
    const [theme,setTheme] = useState("dark")

    return (

        // 使用ThemeContext将响应式的主题数据透传全局
        <ThemeContext.Provider value={themes[theme]}>
            <div>
                我是爹{" "}
                {/* 点击按钮切换全局主题状态 一旦状态发生变化 全局订阅Context的组件都会随之更新 */}
                <button onClick={()=>setTheme(theme==="dark"?"light":"dark")}>切换全局主题</button>
                <br />
                <Son />
            </div>
        </ThemeContext.Provider>
    )
}

export default UseContextDemo
