import React, { /* useState, */ useReducer } from 'react'

const countReducer = (prevState, action) => {
    console.log("countReducer", prevState, action);
    switch (action.type) {
        case "add":
            return { ...prevState, count: prevState.count + 1 }
        case "sub":
            return { ...prevState, count: prevState.count - 1 }
        default:
            return prevState;
    }
}

const stateTree = {
    count: 123
}

function UseReducerDemo() {
    // const [count, setCount] = useState(0)
    // function addCount() {
    //     setCount(count + 1)
    // }
    // function subCount() {
    //     setCount(count - 1)
    // }

    const [state, dispatch] = useReducer(countReducer, stateTree)

    return (
        <div>
            {state.count}
            <br />
            <button onClick={() => { dispatch({ type: "sub" }) }}>-</button>
            <button onClick={() => { dispatch({ type: "add" }) }}>+</button>
        </div>
    )
}

export default UseReducerDemo
