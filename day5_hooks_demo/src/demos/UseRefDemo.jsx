// rfce
import React, { useRef, useEffect } from 'react'

function useRefDemo(props) {
    const domRef = useRef(null)

    useEffect(
        () => {
            console.log("ComponentDidMount");
            console.log(domRef.current.innerText);
        }
    )

    return (
        <>
            <div>
                <button ref={domRef}>点哥</button>
            </div>
            {console.log("re-rendered")}
        </>
    )
}

export default useRefDemo
