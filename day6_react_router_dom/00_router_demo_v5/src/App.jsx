import logo from './logo.svg';
import './App.css';
import { NavLink, Switch, Route, Redirect, withRouter } from "react-router-dom"
import Home from "./components/Home"
import List from './components/List';
import User from './components/User';

function App(props) {
  props.history.listen(
    (link) => {
      console.log("path=", link.pathname);
    }
  )

  return (
    <div className="App">

      {/* 跳转链接 */}
      <div>
        <NavLink to="/home" className="link">首页</NavLink>
        {/* <NavLink to="/list" className="link">列表</NavLink> */}
        <NavLink to={{ pathname: "/list", query: { filter: "all", page: 12 } }} className="link">列表</NavLink>
        <NavLink to="/user/123" className="link">我的</NavLink>
      </div>

      {/* 组件切换区 */}
      <Switch>
        <Route path="/home" component={Home} />
        <Route path="/list" component={List} />
        <Route path="/user/:id" component={User} />
        <Redirect from="/" exact to="/home" />
      </Switch>

      <button onClick={
        () => {
          props.history.push("/user/456")
        }
      }>点我去个人中心</button>

      <button onClick={
        () => {
          props.history.push({
            pathname: "/list",
            query: {
              filter: "latest",
              page: 34
            }
          })
        }
      }>点我去列表页</button>

    </div>
  );
}

export default withRouter(App);
