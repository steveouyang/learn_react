import React, { Component, Fragment } from 'react'
import { NavLink, Switch, Route, Redirect } from "react-router-dom"
import HomeMine from './HomeMine'
import HomeYours from './HomeYours'

export default class Home extends Component {
    render() {
        return (
            <Fragment>
                <div>Home</div>
                {/* 跳转链接 */}
                <div>
                    <NavLink to="/home/yours" className="link">去你家</NavLink>
                    <NavLink to="/home/mine" className="link">去我家</NavLink>
                </div>

                {/* 组件切换区 */}
                <Switch>
                    <Route path="/home/yours" component={HomeYours} />
                    <Route path="/home/mine" component={HomeMine} />
                    <Redirect from="/home" exact to="/home/yours" />
                </Switch>
            </Fragment>
        )
    }
}
