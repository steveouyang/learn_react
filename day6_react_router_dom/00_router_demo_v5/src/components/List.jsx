import React, { Component } from 'react'
import {withRouter} from "react-router-dom"

class List extends Component {
    componentDidMount(){
        console.log("List componentDidMount");
        console.log("filter=",this.props.location.query.filter);
        console.log("page=",this.props.location.query.page);
    }
    render() {
        return (
            <div>List</div>
        )
    }
}

export default withRouter(List)
