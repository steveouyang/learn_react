import * as React from "react";
import { Routes, Route, Outlet, Link } from "react-router-dom";

export default function Dashboard() {
    return (
        <Routes>
            {/* 使用布局组件包裹子组件 */}
            <Route path="/" element={<DashboardLayout />}>
                <Route index element={<DashboardIndex />} />
                <Route path="messages" element={<Messages />} />
            </Route>
        </Routes>
    );
}

/* Dashboard布局组件 */
function DashboardLayout() {
    return (
        <div>
            {/* 没什么卵用 只是语义上标记这是导航区 */}
            <nav>
                <ul>
                    <li>
                        <Link to="/dashboard">Dashboard Home</Link>
                    </li>
                    <li>
                        <Link to="/dashboard/messages">Messages</Link>
                    </li>
                </ul>
            </nav>

            <hr />

            {/* 点击Link时加载到的内容显示在此 */}
            <Outlet />
        </div>
    );
}

/* Dashboard首页 */
function DashboardIndex() {
    return (
        <div>
            <h2>Dashboard Index</h2>
        </div>
    );
}

/* 消息组件 */
function Messages() {
    return (
        <div>
            <h2>Messages</h2>
            <ul>
                <li>Message 1</li>
                <li>Message 2</li>
            </ul>
        </div>
    );
}
