/**
 * 模拟了通用的auth API, 比如 Firebase.
 */
const fakeAuthProvider = {
    // 记录用户是否已登录
    isAuthenticated: false,

    // 登入
    signin(callback) {
        fakeAuthProvider.isAuthenticated = true;
        setTimeout(callback, 100); // fake async
    },

    // 登出
    signout(callback) {
        fakeAuthProvider.isAuthenticated = false;
        setTimeout(callback, 100);
    },
};

export { fakeAuthProvider };
