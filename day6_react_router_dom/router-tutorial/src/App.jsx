import { Link, Outlet } from "react-router-dom";
import QueryNavLink from "./components/QueryNavLink";
import BrandLink from "./components/BrandLink";
import { useState } from "react";

export default function App() {
  const [brands, setBrands] = useState(
    ["nike", "erke", "lining"]
  )

  return (
    <div>
      <h1>Bookkeeper</h1>

      <nav
        style={{
          borderBottom: "solid 1px",
          paddingBottom: "1rem"
        }}
      >
        <Link to="/invoices">Invoices</Link> |{" "}
        <Link to="/expenses">Expenses</Link> |{" "}
        <QueryNavLink to="/whatever">QueryNavLink</QueryNavLink> |{" "}

        {
          brands.map(brand => (
              <BrandLink key={brand} to="/shoes" brand={brand}>{brand} | </BrandLink>
          ))
        }

      </nav>

      <Outlet />
    </div>
  );
}