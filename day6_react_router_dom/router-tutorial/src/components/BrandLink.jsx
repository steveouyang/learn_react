import { Link, useSearchParams } from "react-router-dom";

export default function BrandLink({ to, brand, ...props }) {
    let [params] = useSearchParams();
    let isActive = params.getAll("brand").includes(brand);
    
    return (
        <Link
            style={{ color: isActive ? "red" : "" }}
            to={to + `?brand=${brand}`}
            {...props}
        />
    );
}