import { useParams, useNavigate } from "react-router-dom";
import { getInvoiceByNumber, deleteInvoice } from "../data";

export default function Invoice() {
    const navigate = useNavigate()

    let params = useParams();
    let invoice = getInvoiceByNumber(parseInt(params.invoiceId, 10));
    return (
        <main style={{ padding: "1rem" }}>
            <h2>Total Due: {invoice.amount}</h2>
            <p>
                {invoice.name}: {invoice.number}
            </p>
            <p>Due Date: {invoice.due}</p>
            <p>
                <button onClick={(e) => {
                    deleteInvoice(params.invoiceId * 1)
                    navigate("/invoices")
                }}>撕毁</button>
            </p>
        </main>
    );
}