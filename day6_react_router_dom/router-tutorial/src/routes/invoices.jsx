import { Link, Outlet, NavLink, useSearchParams } from "react-router-dom";
import { getInvoices } from "../data";

export default function Invoices() {
    let invoices = getInvoices();
    const [searchParams, setSearchParams] = useSearchParams()

    return (
        <div style={{ display: "flex" }}>
            <nav
                style={{
                    borderRight: "solid 1px",
                    padding: "1rem"
                }}
            >
                <input type="text"
                    value={searchParams.get("filter") || "骚年请输入发票名称..."}
                    onChange={(e) => {
                        setSearchParams(
                            e.target.value ? { filter: e.target.value } : {}
                        )
                    }}
                />

                {
                    invoices
                        .filter(invoice => {
                            // ?filter=S
                            let f = searchParams.get("filter")
                            return !f ? true : invoice.name.startsWith(f)
                        })
                        .map(invoice => (
                            <NavLink
                                style={({ isActive }) => ({
                                    display: "block",
                                    margin: "1rem 0",
                                    color: isActive ? "red" : ""
                                })
                                }
                                to={`/invoices/${invoice.number}`}
                                key={invoice.number}
                            >
                                {invoice.name}
                            </NavLink>
                        ))
                }
            </nav>

            <Outlet />
        </div>
    );
}