let nextTodoId = 0;

/* 想执行添加todo的时候 dispatch(   addTodo("撸无码")   ) */
export const addTodo = (text) => {
    return {
        type: "ADD_TODO",
        id: nextTodoId++,
        text,
    };
};

/* 想切换任务状态时：dispatch(   toggleTodo(3)   ) */
export const toggleTodo = (id) => {
    return {
        type: "TOGGLE_TODO",
        id,
    };
};

/* 想切换列表过滤器时：dispatch( setVisibilityFilter("SHOW_ALL") ) */
export const setVisibilityFilter = (filter) => {
    return {
        type: "SET_VISIBILITY_FILTER",
        filter,
    };
};
