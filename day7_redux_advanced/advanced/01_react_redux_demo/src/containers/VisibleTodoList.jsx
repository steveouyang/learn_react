import { connect } from 'react-redux'
import { toggleTodo } from '../actions'
import TodoList from '../components/TodoList'

const getVisibleTodos = (todos, filter) => {
    switch (filter) {
        case 'SHOW_COMPLETED':
            return todos.filter(t => t.completed)
        case 'SHOW_ACTIVE':
            return todos.filter(t => !t.completed)
        case 'SHOW_ALL':
        default:
            return todos
    }
}

/* 定义TodoList的局部数据订阅 */
const mapStateToProps = state => {
    return {
        todos: getVisibleTodos(state.todos, state.visibilityFilter)
    }
}

/* 指定的action的派发动作映射为组件的功能函数 */
const mapDispatchToProps = dispatch => {
    return {
        onTodoClick: id => {
            dispatch( toggleTodo(id) )
        }
    }
}

const VisibleTodoList = connect(
    mapStateToProps, //todos
    mapDispatchToProps //onTodoClick
)(TodoList)

export default VisibleTodoList