import React from "react";
import ReactDOM from "react-dom";

// 引入全局Provider
import { Provider } from "react-redux";
import { createStore } from "redux";

// 引入全局reducer
import todoApp from "./reducers";
import App from "./components/App";

// 以todoApp作为全局reducer 创建全局数据仓库
let store = createStore(todoApp);

ReactDOM.render(
    // 将数据仓库透传所有后代组件
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById("root")
);
