import { combineReducers } from "redux";

/* 导入所有的reducer */
import todos from "./todos";
import visibilityFilter from "./visibilityFilter";

/* 全局状态树形态 */
/* 
{
    visibilityFilter:"SHOW_ALL",
    todos:[
        {
            id: action.id,
            text: action.text,
            completed: false,
        }
    ]
}
*/

/* 合并并返回集成的reducer */
const todoApp = combineReducers({
    todos,
    visibilityFilter,
});

export default todoApp;
