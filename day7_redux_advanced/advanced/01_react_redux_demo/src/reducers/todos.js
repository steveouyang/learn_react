const todos = (prevState = [], action) => {
    console.log("reducer todos", prevState, action);

    switch (action.type) {
        // 添加一项todo
        case "ADD_TODO":
            // return新的todos 覆盖prevState
            return [
                ...prevState,
                {
                    id: action.id,
                    text: action.text,
                    completed: false,
                },
            ];

        // 切换指定todo的状态（active/completed）
        case "TOGGLE_TODO":
            return prevState.map((todo) =>
                todo.id === action.id
                    ? { ...todo, completed: !todo.completed }
                    : todo
            );

        // 事实上处理数据的获取请求
        default:
            console.log("直接返回当前的prevState");
            return prevState;
    }
};

export default todos;
