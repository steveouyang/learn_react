const visibilityFilter = (prevState = "SHOW_ALL", action) => {
    console.log("reducer visibilityFilter", prevState, action);
    switch (action.type) {
        case "SET_VISIBILITY_FILTER":
            return action.filter;
        default:
            return prevState;
    }
};

export default visibilityFilter;
