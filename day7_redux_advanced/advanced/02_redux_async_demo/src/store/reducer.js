import { SET_HOT, SET_FETCHING } from "./actions";
import { combineReducers } from "redux";

/* 
{
    isFetching:false,
    hotCities:["北京", "上海", "广州"]
}
*/

function hotCities(prevState = ["北京", "上海", "广州"], action) {
    switch (action.type) {
        case SET_HOT:
            return action.cities;
            
        default:
            return prevState;
    }
}

function isFetching(prevState = false, action) {
    switch (action.type) {
        case SET_FETCHING:
            return action.value;

        default:
            return prevState;
    }
}

const rootReducer = combineReducers({ isFetching, hotCities });
export default rootReducer;
