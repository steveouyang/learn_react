import logo from "./logo.svg";
import "./App.css";
import DataGetter from "./components/DataGetter";
import DataSetter from "./components/DataSetter";

function App() {
    return (
        <div className="App">
            <DataGetter />
            <hr />
            <DataSetter />
        </div>
    );
}

export default App;
