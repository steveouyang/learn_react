import React, { Component } from 'react'
import store from '../store';

export default class DataGetter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            count: store.getState()
        }
    }

    componentDidMount() {
        // 可以手动订阅更新，也可以事件绑定到视图层。
        store.subscribe(() => {
            console.log(store.getState())
            this.setState({
                count: store.getState()
            })
        });
    }

    render() {
        return (
            <div>DataGetter: {this.state.count}</div>
        )
    }
}
