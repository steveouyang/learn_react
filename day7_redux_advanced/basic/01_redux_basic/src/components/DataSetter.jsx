import React, { Component } from 'react'
import store from '../store';

export default class DataSetter extends Component {
    render() {
        return (
            <div>
                DataSetter
                <br />
                
                <button onClick={() => {
                    store.dispatch({ type: 'DECREMENT' });
                }}>-</button>

                <button onClick={() => {
                    store.dispatch({ type: 'INCREMENT' });
                }}>+</button>
            </div>
        )
    }
}
