import React, { Component } from 'react'
import store from '../store';

export default class DataGetter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: null
        }
    }

    componentDidMount() {
        /* 挂在完毕后手动同步一下数据 */
        const data = store.getState()
        this.setState({
            data: data
        })

        /* 订阅全局状态更新 */
        store.subscribe(() => {
            this.setState({
                data: store.getState()
            })
        })
    }

    render() {
        console.log("render data=", this.state.data);
        return (
            <div /* style={{ display: this.state.data ? "block" : "none" }} */>
                DataGetter
                <br />

                filter: {this.state.data && this.state.data.visibilityFilter}
                <br />

                <ul>
                    {
                        this.state.data && this.state.data.todos
                        // .filter(
                        //     (todo) => {
                        //         return this.state.data.visibilityFilter === "SHOW_ALL" ? true : (todo.completed)
                        //     }
                        // )
                        .map(
                                (todo, index) => {
                                    return (
                                        <li key={todo.text + index}>
                                            <p>{todo.text}</p>
                                            <p>{todo.completed ? "搞定" : "没搞定"}</p>
                                        </li>
                                    )
                                }
                            )
                    }
                </ul>
            </div>
        )
    }
}
