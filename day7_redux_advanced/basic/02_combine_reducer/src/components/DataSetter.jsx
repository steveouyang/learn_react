import React, { Component } from 'react'
import store from '../store'

export default class DataSetter extends Component {
    render() {
        return (
            <div>
                DataSetter
                <br />

                <button onClick={() => {
                    store.dispatch({
                        type: 'COMPLETE_TODO',
                        index: 1
                    })
                }}>COMPLETE_TODO</button>

                <button onClick={() => {
                    store.dispatch({
                        type: 'SET_VISIBILITY_FILTER',
                        filter: 'SHOW_COMPLETED'
                    })
                }}>SHOW_COMPLETED</button>

                <button onClick={() => {
                    store.dispatch({
                        type: 'SET_VISIBILITY_FILTER',
                        filter: 'SHOW_ALL'
                    })
                }}>SHOW_ALL</button>

            </div>
        )
    }
}
