import { combineReducers, createStore } from "redux";

const businessModel = {
    visibilityFilter: "SHOW_ALL",
    todos: [
        {
            text: "Consider using Redux",
            completed: true,
        },
        {
            text: "Keep all state in a single tree",
            completed: false,
        },
    ],
};

function visibilityFilter(state = businessModel.visibilityFilter, action) {
    switch (action.type) {
        case "SET_VISIBILITY_FILTER":
            return action.filter;
        default:
            return state;
    }
}

function todos(state = businessModel.todos, action) {
    switch (action.type) {
        case "ADD_TODO":
            return [
                ...state,
                {
                    text: action.text,
                    completed: false,
                },
            ];
        case "COMPLETE_TODO":
            return state.map((todo, index) => {
                if (index === action.index) {
                    return Object.assign({}, todo, {
                        completed: true,
                    });
                }
                return todo;
            });
        default:
            return state;
    }
}

let reducer = combineReducers({ visibilityFilter, todos });
let store = createStore(reducer);

export default store;
