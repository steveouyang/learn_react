import logo from "./logo.svg";
import "./App.css";
import PortalDemo from "./demos/PortalDemo";
import LazyDemo from "./demos/LazyDemo";
import ScuDemo from "./demos/ScuDemo";
import HocDemo from "./demos/HocDemo";
import RenderProp from "./demos/RenderProp";

function App() {
    return (
        <div className="App">
            {/* <PortalDemo>
                <h3>电量不足</h3>
                <p>请尽快充电</p>
            </PortalDemo> */}
            {/* <LazyDemo /> */}
            {/* <ScuDemo /> */}
            {/* <HocDemo name="张半蛋" /> */}
            <RenderProp name="张双蛋" />
        </div>
    );
}

export default App;
