import React, { Component } from 'react'

const withMouse = (Com) => {
    class newCom extends Component {
        constructor(props) {
            super(props);
            this.state = {
                x: 0,
                y: 0
            }
        }

        onMouseMove = (e) => {
            this.setState({
                x: e.pageX,
                y: e.pageY
            })
        }

        render() {
            return (
                <div onMouseMove={this.onMouseMove}>
                    {/* <Com {...this.props} x={this.state.x} y={this.state.y} /> */}
                    <Com {...this.props} {...this.state} />
                </div>
            )
        }
    }
    return newCom
}

class App extends Component {
    render() {
        return (
            <div style={{ height: "500px", backgroundColor: "#eee" }}>
                {this.props.name}
                <br />
                鼠标实时位置：({this.props.x},{this.props.y})
            </div>
        )
    }
}

export default withMouse(App)
