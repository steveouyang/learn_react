import React, { Component } from 'react'
// import ContextDemo from "./components/ContextDemo"

/* 异步引入的一个组件 */
const ContextDemo = React.lazy(
    () => import("./components/ContextDemo")
)

export default class LazyDemo extends Component {
    render() {
        // 加载的过程中显示<div>Loading...</div>
        return (
            <React.Suspense fallback={<div>Loading...</div>}>
                <ContextDemo />
            </React.Suspense>
        )
    }
}
