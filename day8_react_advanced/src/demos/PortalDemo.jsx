import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import "./modal_dialog.css"


export default class ModalDialog extends Component {
    render() {
        // return (
        //     <div>劳资是PortalDemo</div>
        // )

        /* 将JSX渲染到指定的DOM节点 */
        return ReactDOM.createPortal(
            <div className="modal">
                劳资是PortalDemo
                {this.props.children}
            </div>,
            document.body
        )
    }
}
