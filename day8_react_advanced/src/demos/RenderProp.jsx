import React, { Component } from 'react'

/* 对鼠标移动的state与事件的封装提取 */
class Mouse extends Component {
    constructor(props) {
        super(props);
        this.state = {
            x: 0,
            y: 0
        }
    }

    onMouseMove = (e) => {
        this.setState({
            x: e.pageX,
            y: e.pageY
        })
    }

    render() {
        return (
            <div onMouseMove={this.onMouseMove}>
                {this.props.render(this.state.x, this.state.y)}
            </div>
        )
    }
}

class App extends Component {
    render() {
        return (
            <div>
                {this.props.name}
                <br />
                <Mouse render={(x, y) => {
                    return (
                        <div style={{ height: "1000px", backgroundColor: "#eee" }}>鼠标实时位置：{x},{y}</div>
                    )
                }} />
            </div>
        )
    }
}

export default App
