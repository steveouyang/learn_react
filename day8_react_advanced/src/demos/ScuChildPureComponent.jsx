import React, { Component,PureComponent } from 'react'
import _ from 'lodash'

export default class ScuChild extends PureComponent{
    constructor(props) {
        super(props);
        this.state = {
            count: 0,
            obj: {
                name: "张全蛋",
                age: 18
            }
        }
    }

    // shouldComponentUpdate(nextProps, nextState) {
    //     console.log(nextProps, nextState);
    //     // if (nextState.count != this.state.count) {
    //     //     return true
    //     // } else {
    //     //     return false
    //     // }

    //     if (_.isEqual(nextState.obj, this.state.obj)) {
    //         return false
    //     }
    //     return true
    // }


    render() {
        console.log("PureComponent render");
        return (
            <div>
                {this.state.count} <br />
                {JSON.stringify(this.state.obj)}
                <button onClick={() => {
                    this.setState({
                        // count: this.state.count + 1
                        obj: {
                            name: this.state.obj.name,
                            age: this.state.obj.age + 1
                        }
                    })
                }}>+</button>
            </div>
        )
    }
}
