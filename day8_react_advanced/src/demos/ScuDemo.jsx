import React, { Component } from 'react'
import ScuChild from './ScuChild'
import ScuChildPureComponent from './ScuChildPureComponent'

export default class ScuDemo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            time: 0
        }
    }

    render() {
        return (
            <div>
                父组件状态: {this.state.time}
                <button onClick={() => {
                    this.setState({
                        time: Date.now()
                    })
                }}>更新时间</button>
                <hr />
                <ScuChild />
                <hr />
                <ScuChildPureComponent />
            </div>
        )
    }
}
